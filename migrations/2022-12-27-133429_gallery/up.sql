CREATE TABLE IF NOT EXISTS images (
    id SERIAL UNIQUE,
    path VARCHAR(150) NOT NULL,
    width INTEGER NOT NULL,
    height INTEGER NOT NULL,
    title VARCHAR(200) NOT NULL,
    private BOOLEAN DEFAULT FALSE,
    content bytea NOT NULL
);

CREATE INDEX IF NOT EXISTS images_search_index
    ON images
    USING gin(to_tsvector('simple', title)
);

CREATE TABLE IF NOT EXISTS comments (
    image_id INTEGER,
    user_name VARCHAR(150),
    comment VARCHAR(300),
    CONSTRAINT fk_comment_image
    FOREIGN KEY (image_id)
    REFERENCES images(id)
);

CREATE TABLE IF NOT EXISTS metadata (
    image_id INTEGER,
    creationTime FLOAT,
    camera_make VARCHAR(10000),
    camera_model VARCHAR (10000),
    orientation INTEGER,
    horizontal_ppi INTEGER,
    vertical_ppi INTEGER,
    shutter_speed FLOAT,
    color_space VARCHAR(20),
    CONSTRAINT fk_metadata_images
    FOREIGN KEY (image_id)
    REFERENCES images(id)
);
