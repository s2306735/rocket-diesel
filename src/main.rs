#[macro_use] extern crate rocket;
#[macro_use] extern crate diesel_migrations;

use rocket::figment::{value::{Map, Value}, util::map};
use rocket::{Build, Rocket};
use rocket::fairing::AdHoc;
use rocket_sync_db_pools::{database, diesel};
use rocket_sync_db_pools::diesel::PgConnection;

#[database("postgres")]
struct MyPgDatabase(diesel::PgConnection);

async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build>  {
    use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
    const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations");

    MyPgDatabase::get_one(&rocket).await
        .expect("Database mounted")
        .run(|conn: &mut PgConnection| { conn.run_pending_migrations(MIGRATIONS).expect("Diesel migrations"); })
        .await;

    rocket
}

fn get_database_url() -> String {
    if let Ok(url) = std::env::var("DATABASE_URL") {
        return url;
    } else {
        return String::from("postgres://postgres:rocket@127.0.0.1/postgres")
    }
}

#[launch]
fn rocket() -> _ {
    let db: Map<_, Value> = map! {
        "url" => get_database_url().into()
    };
    let figment = rocket::Config::figment()
        .merge(("databases", map!["postgres" => db]));

    rocket::custom(figment)
        .attach(MyPgDatabase::fairing())
        .attach(AdHoc::on_ignite("Database Migrations", run_migrations))
        .mount("/", routes![])
}
